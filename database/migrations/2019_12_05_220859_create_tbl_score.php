<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->bigInteger('student_id')->unsigned() ;
            $table->bigInteger('subject_id')->unsigned() ;
            $table->float('score') ;
            $table->engine = 'InnoDB';
        });
    }

     /* @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
