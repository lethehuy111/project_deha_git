<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker-> name,
        'email'=> $faker->unique()->email,
        'phone' =>   $faker->unique()->e164PhoneNumber,
        'address_id'=> $faker->numberBetween(1,9),
        'birthday'=>$faker->date('Y-m-d'),
        'specialize_id'=>$faker->numberBetween(1,4),
        'gender' => $faker->numberBetween(0,1),
    ];
});
