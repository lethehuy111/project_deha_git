<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialize extends Model
{
    protected $table = 'specializes';
    protected $fillable = [
        'name_specialize'
    ];
    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('App\User', 'specialize_id', 'id');
    }

    public function getAll()
    {
        return $this->withCount(['user'])->get();
    }

    public function getUserFromSpecialize($id)
    {
        return $this->where('id', $id)->with('user')->first();
    }

}
