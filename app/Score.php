<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $table = "scores";
    protected $fillable = [
        "student_id",
        "subject_id",
        "score"
    ];
    public $timestamps = false;

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function getAllScore($id_student, $id_subject)
    {
        return $this->where('student_id', $id_student)->where('student_id', '>', 1)
            ->where('subject_id', $id_subject)->first();
    }
}
