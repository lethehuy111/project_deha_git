<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address_id',
        'birthday',
        'specialize_id',
        'gender',
        'password',
        'is_admin',
        'status'
    ];
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function address()
    {
        return $this->belongsTo('App\Address', 'address_id');
    }

    public function specialize()
    {
        return $this->belongsTo('App\Specialize', 'specialize_id');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject', 'scores', 'student_id',
            'subject_id')
            ->withPivot('score');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    public function getAll()
    {
        return $this->with(['address', 'specialize'])->where('status', '0')
            ->where('is_admin', '0')->orderBy('id', 'desc')->get();
    }

    public function restoreBy($id)
    {
        $user = $this->where('id', $id)->restore();
        return $this->getOneStudent($id);
    }

    public function search($data)
    {
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $gender = $data['gender'];
        $address_id = $data['address_id'];
        $specialize_id = $data['specialize_id'];
        return $this->with(['address', 'specialize'])->where('name', 'like', '%' . $name . '%')
            ->where('email', 'like', '%' . $email . '%')
            ->where("is_admin", 0)
            ->where('phone', 'like', '%' . $phone . '%')
            ->where('gender', 'like', '%' . $gender . '%')
            ->when($address_id, function ($query) use ($address_id) {
                $query->where('address_id', $address_id);
            })->when($specialize_id, function ($query) use ($specialize_id) {
                $query->where('specialize_id', $specialize_id);
            })->orderBy('id', 'desc')->get();
    }

    public function getScoreBySubjectId($user_id, $subject_id)
    {
        $score = $this->with(['subjects' => function ($query) use ($subject_id) {
            $query->where('id', $subject_id);
        }])->findOrFail($user_id);
        return $score->subjects[0]->pivot;
    }

    public function getStudentById($id)
    {
        return $this->with(['address', 'specialize'])->findOrFail($id);
    }

    public function countStudentOfSpecialize($id)
    {
        return $this->where('specialize_id', $id)->where('is_admin', 0)->count();
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn("name", $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where("name", $role)->first();
    }

    public function checkRoleDeleteUser()
    {
//        $id = Auth::id();
        $id = 1;
        $user = $this->find($id);
        foreach ($user->roles as $role) {
            if ($role->name === 'Admin') {
                return true;
            }
            return false;
        }
    }
}
