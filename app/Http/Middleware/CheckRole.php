<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckRole
{
    protected $user;
    function __construct()
    {
        $this->user = new User();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->user()->authorizeRoles(['admin']);

        return $next($request);
    }
}
