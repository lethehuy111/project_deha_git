<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     **/
    public function index()
    {
        return view('login');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function authenticate(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $email = $request->input('email');
        $password = $request->input('password');
        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            $info = "Tài khoản hoặc mật khẩu chưa đúng";
            return view('login', compact('info','email','password'));
        }
    }
}
