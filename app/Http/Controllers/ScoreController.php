<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Score;
use App\Specialize;
use App\User;
use App\Http\Requests\ScoreRequest;

class ScoreController extends Controller
{
    protected $subject;
    protected $score;
    protected $specialize;
    protected $user;

    function __construct(
        Subject $subject,
        Specialize $specialize,
        Score $score,
        User $user
    )
    {
        $this->subject = $subject;
        $this->specialize = $specialize;
        $this->score = $score;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = $this->subject->all();
        return view('subject.list-subject', compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScoreRequest $request)
    {
        $subject_id = $request->subject_id;
        $student_id = $request->student_id;
        $score = $request->score;

        $score = $this->subject->createScoreById($subject_id, $student_id, $score);
        return response()->json([
            "data" => $score
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = $this->subject->find($id);
        $nameSubject = $subject->name_subject;
        $students = $this->specialize->getUserFromSpecialize($subject->specialize_id);
        $data = array();
        foreach ($students->user as $key => $student) {
            if ($student->id > 1) {
                $score = $this->score->getAllScore($student->id, $id);
                if ($score != null) {
                    $data[$key]['score'] = $score->score;
                } else if ($student->id) {
                    $data[$key]['score'] = null;
                }
                $data[$key]['name'] = $student->name;
                $data[$key]['name'] = $student->name;
                $data[$key]['student_id'] = $student->id;
                $data[$key]['subject_id'] = $id;
            }
        }
        return view('score.index', compact('data', 'nameSubject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user_id = $request->student_id;
        $subject_id = $request->subject_id;
        $score = $this->user->getScoreBySubjectId($user_id, $subject_id);
        return response()->json([
            'data' => $score
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScoreRequest $request, $id)
    {
        $subject_id = $request->subject_id;
        $student_id = $request->student_id;
        $score = $request->score;
        $score = $this->subject->updateScoreById($subject_id, $student_id, $score);
        return response()->json([
            "data" => $score
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $subject_id = $request->subject_id;
        $student_id = $request->student_id;
        $score = $this->subject->detachScoreById($subject_id, $student_id);
        return response()->json([
            "message" => "Xóa thành công!"
        ]);

    }
}
