<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Requests\RequestApi;
use Validator;
use App\User;

class ApiController extends Controller
{
    protected $user;

    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->all();
        return response()->json([
            'status_code' => '200',
            'message' => 'Danh sách người dùng',
            'users' => $users
        ]);
    }

    public function validateApi($request, $id)
    {

        $rules = [
            'name' => 'required|min:3|max:40',
            'email' => 'required|email|unique:users,email,' . $id,
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|unique:users,phone,' . $id,
            'address_id' => 'required|alpha_num',
            'birthday' => 'required',
            'gender' => 'required',
            'specialize_id' => 'required|alpha_num'


        ];
        $messages = [
            'name.required' => 'Không để trống tên sinh viên',
            'name.min' => 'Tên sinh viên ít nhất là 3 ký tự',
            'name.max' => 'Tên sinh viên quá dài ',

            'email.required' => 'Không để trống email',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại',

            'phone.required' => 'Không để trống số điện ',
            'phone.regex' => 'Số điện thoại không đúng định dạng',
            'phone.unique' => 'Số điện thoại đã tồn tại',

            'address_id.required' => 'Không để trống địa chỉ ',

            'birthday.required' => 'Không để trống Ngày sinh',

            'specialize_id.required' => 'Không để trống chuyen nganh',

            'gender.required' => 'Không để trống giới tính',

        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json([
                'status_code' => '400',
                'message_error' => $validator->errors()
            ]);
        }
    }

    public function store(Request $request)
    {
        $error_validate = $this->validateApi($request, null);
        if ($error_validate) {
            return $error_validate;
        }
        $user = $this->user->create($request->all());
        return response()->json([
            'status_code' => '201',
            'message' => 'Thêm người dùng thành công',
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $error_validate = $this->validateApi($request, $id);
        if ($error_validate) {
            return $error_validate;
        }
        $user->update($request->all());
        return response()->json([
            'status_code' => '200',
            'message' => 'Cập nhật thành công ',
            'user' => $user
        ]);
    }

    public function show($id)
    {
        $user = $this->user->findOrFail($id);
        return response()->json([
            'status_code' => '200',
            'message' => 'Yêu cấu thành công',
            'user' => $user
        ]);
    }

    public function destroy($id)
    {
        if ($this->user->checkRoleDeleteUser() == true) {
            $user = $this->user->findOrFail($id);
            $user->delete();
            return response()->json([
                'status' => '200',
                'message' => 'Đã xóa thành công'
            ]);
        } else {
            return response()->json([
                'messager' => 'Bạn không có quyền !',
            ], 403);
        }

    }
}
