<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialize;
use App\User;
use App\Http\Requests\SpecializeRequest;
use Illuminate\Http\Response;

class SpecializeController extends Controller
{
    protected $user;
    protected $specialize;

    function __construct(
        User $user,
        Specialize $specialize
    )
    {
        $this->user = $user;
        $this->specialize = $specialize;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $specializes = $this->specialize->getAll();
        return view('specialize/index', compact('specializes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(SpecializeRequest $request)
    {
        $specialize = $this->specialize->create($request->all());
        return response()->json(['message' => $specialize]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $specialize = $this->specialize->find($id);
        return response()->json([
            'data' => $specialize
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(SpecializeRequest $request, $id)
    {
        $specialize = $this->specialize->find($id);
        $specialize->update($request->all());
        return response()->json([
            "data" => $specialize
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $numberStudent = $this->user->countStudentOfSpecialize($id);
        if ($numberStudent > 0) {
            return false;
        } else {
            $delete = $this->specialize->destroy($id);
            return response()->json(["message" => "Xóa Thành Công"]);
        }
    }
}
