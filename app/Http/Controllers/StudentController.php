<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\User;
use App\Address;
use App\Specialize;

class StudentController extends Controller
{
    protected $address;
    protected $subject;
    protected $specialize;
    protected $user;

    function __construct(
        Address $address,
        Subject $subject,
        Specialize $specialize,
        User $user
    )
    {
        $this->address = $address;
        $this->subject = $subject;
        $this->specialize = $specialize;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specializes = $this->specialize->getAll();
        $address = $this->address->getAll();
        $students = $this->user->getAll();
        return view('student/index', compact('students', 'address', 'specializes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $address = $this->address->getAll();
        $specializes = $this->specialize->getAll();
        return view('student/add', compact('address', 'specializes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = $this->address->getAll();
        $student = $this->user->getStudentById($id);
        $specializes = $this->specialize->getAll();
        return view('student/edit', compact('address', 'specializes', 'student'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function restore($id)
    {
        $user = $this->user->restoreBy($id);
        return response()->json([
            'data' => $user
        ]);
    }

    public function search(Request $request)
    {
        $data = array();
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
            $data['phone'] = $request->input('phone');
        $data['address_id'] = $request->input('address_id');
        $data['specialize_id'] = $request->input('specialize_id');
        $data['gender'] = $request->input('gender');
        $users = $this->user->search($data);
        return response()->json(["data" => $users]);
    }
}
