<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'score' => 'required|numeric|min:0|max:10',
        ];
    }

    public function messages()
    {
        return [
            'score.required' => 'Không được để trống điểm',
            'score.numeric' => 'Điểm nhập vào phải là số',
            'score.min' => 'Số nhập vào nhỏ nhất là số 0',
            'score.max' => 'Số nhập vào lớn nhất là 10',
        ];
    }
}
