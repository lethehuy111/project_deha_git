<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\lengthStringRule;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = ($request->id) ? $request->id : "";
        return [
            'name_subject' => ['required', 'unique:subjects,name_subject,' . $id, new lengthStringRule()],
        ];
    }

    public function messages()
    {
        return [
            'name_subject.required' => 'Không bỏ trống tên môn học',
            'name_subject.unique' => 'Tên môn học đã tồn tại'
        ];
    }
}
