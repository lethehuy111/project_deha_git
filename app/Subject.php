<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Array_;

class Subject extends Model
{
    //
    protected $table = 'subjects';
    protected $fillable = [
        "name_subject",
        "specialize_id"
    ];
    public $timestamps = false;

    public function Specialize()
    {
        return $this->belongsTo('App\Specialize');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'scores', 'subject_id', 'student_id')
            ->withPivot('score');
    }

    public function getSubjectById($id)
    {
        return $this->where('specialize_id', $id)->get();
    }

    public function createScoreById($subject_id, $student_id, $score)
    {
        $subject = $this->find($subject_id);
        $subject->users()->attach($student_id, ['score' => $score]);
        return $subject;
    }

    public function updateScoreById($subject_id, $student_id, $score)
    {
        $subject = $this->find($subject_id);
        $subject->users()->detach($student_id);
        return $subject->users()->attach($student_id, ['score' => $score]);
    }

    public function detachScoreById($subject_id, $student_id)
    {
        $subject = $this->find($subject_id);
        $subject->users()->detach($student_id);
        return $subject;
    }
}
