$(function () {
    countRowTable();
    // filter table
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    //add specialize
    $('.add-specialize').on("click", function (e) {
        e.preventDefault();
        $('.error-name').html("");
        var data = $('.form-add').serialize();
        callAjax("/specializes", "POST", data)
            .done(function (data) {
                $('#addSpecialize').modal('hide');
                swal("Thêm Mới Thành Công!", "", "success");

                var htmlTable = `<tr class="tr${data.message.id}">
                    <td><strong></strong></td>
                    <td>${data.message.name_specialize}</td>
                    <td >0</td>
                    <td>
                        <a class="btn btn-primary" href="/subjects/${data.message.id}" >Môn học</a>
                        <button value="${data.message.id}" class="btn btn-success action edit">Sửa</button>
                        <button value="${data.message.id}" class="btn btn-danger action delete">Xóa</button>
                    </td>
                </tr>`;
                $('.table-specialize').append(htmlTable);
                $('.form-add')[0].reset();
                countRowTable();
            })
            .fail(function (error) {
                $('.error-name').html(error.responseJSON.errors.name_specialize);
            })
    });
    //delele specialize

    $(document).on("click", ".delete", function () {
        var id = $(this).val();
        swal({
            title: "Bạn chắc chắn muốn xóa khoa này không?",
            text: "Khi xóa sẽ không khôi phục lại được nữa!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                    callAjax("/specializes/" + id, "DELETE", id)
                        .done(function (data) {
                            swal("Xóa Thành Công!", "", "success");
                            $('.tr' + id).remove();
                            countRowTable();
                        })
                        .fail(function () {
                            swal("Xóa Không Thành Công!", "Vẫn Còn học sinh trong khoa!", "error");
                        });
                }
            });
    });
    //edit specialize
    $(document).on("click", ".edit", function () {
        let id = $(this).val();
        callAjax('/specializes/' + id, 'GET', id)
            .done(function (data) {
                console.log(data);
                $(".input-specialize").val(data.data.name_specialize);
                $(".id-specialize").val(data.data.id);
                $(".edit-specialize").val(data.data.id);
                $(".error-edit-name").html("");
                $("#editSpecialize").modal('show');
            })
    });
    $(document).on("click", ".edit-specialize", function (e) {
        e.preventDefault();
        let id = $(this).val();
        let formData = {
            name_specialize: $(".input-specialize").val()
        };
        callAjax('/specializes/' + id, 'PUT', formData)
            .done(function (response) {
                console.log(response);
                swal("Thay Đổi Thành Công!", "", "success")
                    .then((value) => {
                        $(".td" + id).html(response.data.name_specialize);
                        $("#editSpecialize").modal('hide');
                        $('.form-edit').trigger("reset");
                    });
            })
            .fail(function (error) {
                $('.error-edit-name').html(error.responseJSON.errors.name_specialize);
            })
    })
});
