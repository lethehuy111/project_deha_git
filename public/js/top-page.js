$(function () {
    countRowTable();
    getPagination('#table-id');
    $('.search').on("click", function () {
        let checkValue = $('.search').val();
        if (checkValue) {
            $('.content-search').slideDown();
            $('.search').val("");
        } else {
            $('.content-search').slideUp();
            $('.search').val(1);
        }
    });

    function addRecordTable(data) {
        let tableHtml = ` <tr class="tr${data.id}">
                        <td><strong></strong></td>
                        <td style="width: 200px;">${data.name}</td>
                        <td style="width: 200px;">${data.email} </td>
                        <td>${data.phone}</td>
                        <td>${(data.gender == 1) ? "Nam" : "Nữ"}</td>
                        <td>${data.address.name_city}</td>
                        <td>${data.specialize.name_specialize}</td>
                        <td>
                            <button value="${data.id}" class="btn btn-danger action delete">Xóa</button>
                            <a class="btn btn-success action" href="http://localhost/students/${data.id}/edit"> Sửa</a>
                        </td>`;
        return tableHtml;
    }

    $(document).on("click", ".delete", function () {
        // Bật sweetalert
        let id = $(this).val();
        swal({
            title: "Bạn chắc chắn muốn xóa sinh viên này?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                    callAjax("/api/users/" + id, 'DELETE', id)
                        .done(function (data) {
                            $('.alert-dark').slideDown();
                            $('.restore').val(id);
                            $('.tr' + id).remove();
                            countRowTable();
                            $('.alert-dark').delay(8000).slideUp();
                        })
                        .fail(function (error) {
                            //console.log(error);
                            if (error.responseJSON.messager) {
                                swal(error.responseJSON.messager, "", "warning");
                            } else {
                                swal("Xóa Không Thành Công!", "", "warning");
                            }
                        })
                }
            });
    });
    $(document).on("click", ".restore", function () {
        let id = $(this).val();
        callAjax('/users/restore/' + id, 'GET', null)
            .done(function (response) {
                $('.alert-dark').hide();
                let tableHtml = addRecordTable(response.data);
                $('.results-search tbody').prepend(tableHtml);
                countRowTable();
            })
    });
    $('.reset').on("click", function (e) {
        e.preventDefault();
        $(".form-search")[0].reset();
    });
    $('.sm-search').on("click", function (e) {
        e.preventDefault();
        let data = $(".form-search").serialize();
        console.log(data);
        callAjax("/users/search", "GET", data)
            .done(function (response) {
                console.log(response);
                let resultSearch = response.data;
                $('.results-search tbody').html('');
                for (var i = 0; i < resultSearch.length; i++) {
                    let tableHtml = addRecordTable(resultSearch[i]);
                    $('.results-search tbody').append(tableHtml);
                    countRowTable();
                }
                getPagination('#table-id');
                countRowTable();
            })
            .fail(function (error) {
                console.log(error);
            })
    });
});
