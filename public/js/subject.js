$(function () {
    countRowTable();
    // filter table
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    //add specialize
    $('.add-subject').on("click", function (event) {
        event.preventDefault();
        $('.error-name').html("");
        var data = $('.form-add').serialize();
        callAjax("/subjects", "POST", data)
            .done(function (data) {
                console.log(data.message);
                $('#add-subject').modal('hide');
                swal("Thêm Mới Thành Công!", "", "success");
                var htmlTable = `<tr class="tr${data.message.id}">
                    <td><strong></strong></td>
                    <td>${data.message.name_subject}</td>
                    <td>
                        <a class="btn btn-primary" href="http://localhost/scores/${data.message.id}" >Xem điểm</a>
                        <button value="${data.message.id}" class="btn btn-success action edit">Sửa</button>
                        <button value="${data.message.id}" class="btn btn-danger action delete">Xóa</button>
                    </td>
                </tr>`;
                $('.table-subject').append(htmlTable);
                countRowTable();
                $('.form-add')[0].reset() ;
            })
            .fail(function (error) {
                console.log(error);
                $('.error-name').html(error.responseJSON.errors.name_subject);
            })
    });
    //delete subject
    $(document).on("click", ".delete", function () {
        var id = $(this).val();
        swal({
            title: "Bạn chắc chắn muốn xóa khoa này không?",
            text: "Khi xóa sẽ không khôi phục lại được nữa!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                    callAjax("/subjects/" + id, "DELETE", id)
                        .done(function (data) {
                            swal("Xóa Thành Công!", "", "success");
                            $('.tr' + id).remove();
                            countRowTable();
                        })
                        .fail(function () {
                            swal("Xóa Không Thành Công!", "Vẫn Còn học sinh đang học !", "warning");
                        });
                }
            });

    });
    //edit specialize
    $(document).on("click", ".edit", function () {
        let id = $(this).val();
        callAjax('/subjects/' + id + '/edit', 'GET', id)
            .done(function (data) {
                // console.log(data);
                $(".input-subject").val(data.data.name_subject);
                $(".id-subject").val(data.data.id);
                $(".edit-specialize").val(data.data.id);
                $(".id-specialize").val(data.data.specialize_id);
                $(".error-edit-name").html(" ");
                $("#editSpecialize").modal('show');
            })
    });

    $('.edit-specialize').on("click", function (e) {
        e.preventDefault();
        let id = $(this).val();
        let data = $('.form-edit').serialize();
        callAjax('/subjects/' + id, 'PATCH', data)
            .done(function (response) {
                $("#add-subject").modal('hide');
                swal("Thay Đổi Thành Công!", "", "success");
                $(".td" + id).html(response.data.name_subject);
                $("#editSpecialize").modal('hide');
            })
            .fail(function (error) {
               // console.log(error);
                $('.error-edit-name').html(error.responseJSON.errors.name_subject);
            })
    })
});
