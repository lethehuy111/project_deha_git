$(function () {
    countRowTable();
    // filter table
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    //add score
    $('.show-modal-add').on("click", function (e) {
        e.preventDefault();
        let id = $(this).val();
        let student_id = this.getAttribute("student_id");
        let subject_id = this.getAttribute("subject_id");
        $(".student-id").val(student_id);
        $(".subject-id").val(subject_id);
        $('.error-name').html("");
    });
    $('#form-add').on("submit", function (event) {
        event.preventDefault();
        let id = $(this).val();
        let data = $('#form-add').serialize();
        let student_id = $(".student-id").val();
        let score = $(".score-add").val();
        callAjax("/scores", "POST", data)
            .done(function (response) {
                $('#add-score').modal('hide');
                swal("Thêm Mới Thành Công!", "", "success")
                    .then((value) => {
                        $(".add" + student_id).attr("disabled", "disabled");
                        $(".edit" + student_id).val(score);
                        $(".td" + student_id).html(score);
                    });
                $('#form-add')[0].reset();
                countRowTable();
            })
            .fail(function (error) {
                $('.error-name  ').html(error.responseJSON.errors.score);
            });
    });
    //edit score
    $(document).on("click", ".edit", function () {
        let score = $(this).val();
        if (score == "") {
            swal("Không sửa được do chưa có điểm!", "", "error");
        } else {
            let student_id = this.getAttribute("student_id");
            let subject_id = this.getAttribute("subject_id");
            let arr_id = {
                student_id: student_id,
                subject_id: subject_id
            };
            // call Ajax get a score
            callAjax("/scores/" + student_id + "/edit", "GET", arr_id)
                .done(function (response) {
                    //  console.log(response);
                    $(".student_id").val(response.data.student_id);
                    $(".subject_id").val(response.data.subject_id);
                    $(".score").val(response.data.score);
                    $(".error-edit-name").html(" ");
                    $("#edit-score").modal('show');
                })
                .fail(function (error) {
                })
        }
    });

    $('.edit-scorce').on("click", function (e) {
        e.preventDefault();
        let data = $('.form-edit').serialize();
        let student_id = $(".student_id").val();
        let score = $(".score").val();
        //call ajax edit score
        callAjax('/scores/' + student_id, 'PUT', data)
            .done(function (data) {
                swal("Thay đổi thành công Thành Công!", "", "success");
                $("#edit-score").modal('hide');
                $(".td" + student_id).html(score);
            })
            .fail(function (error) {
                $('.error-edit-name').html(error.responseJSON.errors.score[0]);
            })
    });
    $(document).on("click", ".delete", function () {
        let score = $(this).val();
        if (score == "") {
            swal("Xóa Không Thành Công!", "Học sinh này chưa có điểm !", "error");
        } else {
            let student_id = this.getAttribute("student_id");
            let subject_id = this.getAttribute("subject_id");
            swal({
                title: "Bạn chắc chắn muốn xóa điểm sinh viên  này không?",
                text: "Khi xóa sẽ không khôi phục lại được nữa!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        let arr_id = {
                            student_id: student_id,
                            subject_id: subject_id
                        };
                        console.log(arr_id);
                        //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                        callAjax("/scores/" + student_id, "DELETE", arr_id)
                            .done(function (response) {
                                swal("Xóa Thành Công!", "", "success");
                                $(".td" + student_id).html("Chưa có điểm");
                                $(".add" + student_id).removeAttr("disabled");
                                $(".edit" + student_id).val("");
                            })
                            .fail(function (error) {

                            })
                    }
                });
        }
    })
});


