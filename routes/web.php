<?php
//
///*
//|--------------------------------------------------------------------------
//| Web Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register web routes for your application. These
//| routes are loaded by the RouteServiceProvider within a group which
//| contains the "web" middleware group. Now create something great!
//|
//*/
//
Route::get('login' , 'Auth\LoginController@index') ;
Route::post('login', 'Auth\LoginController@authenticate') ;
Route::get('logout' , 'Auth\LoginController@logout') ;
Route::get('users' , 'Auth\LoginController@welcome')->name('user.login');
//
Route::get('/', 'StudentController@index')->middleware('Login');
Route::get('users/search' , 'StudentController@search');
Route::get('users/restore/{id}' , 'StudentController@restore');
////Route::get('/', 'StudentController@index');
Route::resource('students', 'StudentController');
Route::resource('scores' , 'ScoreController');
////Route::get('/', 'StudentController@index');
Route::group(['middleware' => ['Role']], function () {
    Route::resource('specializes','SpecializeController' );
    Route::resource('subjects','SubjectController');
});



