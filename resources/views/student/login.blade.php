<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đăng nhập hệ thống</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/my_css.css')}}">
</head>
<body>
<div class="container">
    <div class="row " style="margin-top: 150px;">
        <form action="" method="POST">
            @csrf
            <legend style="padding-left: 155px;">Đăng nhập hệ thống</legend>
            <div class="tab_login">
                <div class="error">
                    <ul>
                        hiển thị lỗi ở đây
                    </ul>
                </div>
                <div class="input-group">
                    <!-- <span class="input-group-text"><i class="fas fa-key"></i></span> -->
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" class="form-control" required name="user" placeholder="Tài Khoản" value="">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control" required name="pass" value="" placeholder="Mật Khẩu">
                </div>
                <div>
                    <button type="submit" class="btn btn-danger" name="sm_login">Đăng nhập</button>
                </div>
                <div class="register">
                    <a href="">Đăng ký tài khoản</a>/<a href="">Quên mật khẩu ?</a>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- -------------------------------------------------------- -->
</body>
