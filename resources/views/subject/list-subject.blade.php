@extends('layout.app')
@section('title', 'Quản lý môn học')
@section('content')
    <div class="container mt-3">
        <input class="form-control" id="search" type="text" placeholder="Tìm kiếm..." size="10" style="width: 91%">
        <br>
        <table class="table table-hover table-subject">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên môn học</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody id="myTable">
            @foreach($subjects as $subject)
                <tr class="tr{{$subject->id}}">
                    <td><strong></strong></td>
                    <td class="td{{$subject->id}}">{{$subject->name_subject}}</td>
                    <td>
                        <a class="btn btn-success" href="{{asset('scores/'.$subject->id)}}">Xem điểm</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{--        Modal show edit specialize --}}
    </div>
@endsection
@section('LinkJs')
    <script src="{{asset('js/subject.js')}}"></script>
@endsection


