@extends('layout.app')
@section('title', 'Quản lý môn học')
@section('content')
    <div class="container mt-3">
        <input class="form-control" id="search" type="text" placeholder="Tìm kiếm..." size="10">
        <button class="btn btn-success btn-search" data-toggle="modal" data-target="#add-subject">Thêm mới</button>
        <br>
        <table class="table table-hover table-subject">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên môn học</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody id="myTable">
            @foreach($subjects as $subject)
                <tr class="tr{{$subject->id}}">
                    <td><strong></strong></td>
                    <td class="td{{$subject->id}}">{{$subject->name_subject}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{asset('scores/'.$subject->id)}}">Xem điểm</a>
                        <button value="{{$subject->id}}" class="btn btn-success action edit">Sửa</button>
                        <button value="{{$subject->id}}" class="btn btn-danger action delete">Xóa</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- The Modal add-->
        <div class="modal fade" id="add-subject">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới môn học</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" class="form-add">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="specialize_id" value="{{$id}}">
                                <label for="usr">Tên môn học :<span class="error-name"></span></label>
                                <input type="text" class="form-control" name="name_subject">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success add-subject">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
        {{--        Modal show edit specialize --}}
        <div class="modal fade" id="editSpecialize">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Sửa môn học</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" class="form-edit">
                            <div class="form-group">
                                <label for="usr">Tên môn học :<span class="error-edit-name"></span></label>
                                <input type="hidden" class="form-control id-subject" name="id" value="">
                                <input type="hidden" class="form-control id-specialize" name="specialize_id" value="">
                                <input type="text" class="form-control input-subject" name="name_subject">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success edit-specialize">Thay đổi</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('LinkJs')
    <script src="{{asset('js/subject.js')}}"></script>
@endsection

