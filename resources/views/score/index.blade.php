@extends('layout.app')
@section('title', 'Quản lý điểm')
@section('content')
    <div  style="padding-left: 10px;">
        <h2>Danh sách điểm môn {{$nameSubject}}</h2>
    </div>
    <div class="container mt-3" id="score">
        <input class="form-control" id="search" type="text" placeholder="Tìm kiếm..." size="10">

        <br>
        <table class="table table-hover table-score">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên Sinh Viên</th>
                <th>Điểm số</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody id="myTable">
            @foreach($data as $student)
                <tr class="tr">
                    <td><strong></strong></td>
                    <td>{{$student['name']}}</td>
                    <td class="td{{$student['student_id']}}">{{($student['score'])?$student['score']:"Chưa có điểm"}}</td>
                    <td>
                        <button class="btn btn-success show-modal-add add{{$student['student_id']}}"
                                {{($student['score'])?"disabled":""}}
                                student_id={{$student['student_id']}} subject_id
                        = {{$student['subject_id']}} data-toggle="modal" data-target="#add-score">Thêm điểm</button>
                        <button class="btn btn-primary action edit edit{{$student['student_id']}}"
                                value="{{$student['score']}}"
                                student_id={{$student['student_id']}} subject_id
                        = {{$student['subject_id']}} >Sửa</button>
                        <button class="btn btn-danger action delete" value="{{$student['score']}}"
                                student_id={{$student['student_id']}}
                                    subject_id= {{$student['subject_id']}}> Xóa
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- The Modal -->
        <div class="modal fade" id="add-score" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm Điểm</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" id="form-add">
                            <div class="form-group">
                                @csrf
                                <label for="usr">Điểm số :<span class="error-name"></span></label>
                                <input type="hidden" class="form-control student-id" name="student_id">
                                <input type="hidden" class="form-control subject-id" name="subject_id">
                                <input type="text" class="form-control score-add" name="score">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success add-score">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
        {{--        Modal show edit score --}}
        <div class="modal fade" id="edit-score">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sửa điểm </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" class="form-edit">
                            @csrf
                            <div class="form-group">
                                <label for="usr">Điểm số :<span class="error-edit-name"></span></label>
                                <input type="hidden" class="form-control student_id" name="student_id">
                                <input type="hidden" class="form-control subject_id" name="subject_id">
                                <input type="number" class="form-control score" name="score">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success edit-scorce">Thay đổi</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('LinkJs')
    <script src="{{asset('js/scorce.js')}}"></script>
@endsection
